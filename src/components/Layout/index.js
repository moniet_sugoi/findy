import React, { useEffect, useContext } from 'react'
import { StoreContext } from '../../store'
import Header from '../../common/Header'
import Nav from '../../common/Nav'

export default ({ children }) => { 
   const [state, dispatch] = useContext(StoreContext)

   useEffect(() => {
       const loggedIn = localStorage.getItem('loggedIn')

       if (loggedIn === 'true' && !state.loggedIn) {
            dispatch({type: 'SET_LOGGED_IN', payload: true})
            console.log('setting logged in')
       }
   }, []); // eslint-disable-line

   console.log(state.loggedIn)

   return (
        <>
            <Header />
            <div className="container">
                {children}
                <Nav />
            </div>
        </>
   )
}