import React, { useEffect, useContext, useRef, useState } from 'react'
import { StoreContext } from '../../store'

import './Dashboard.scss'
import MasonryImg from './MasonryImg'
import ImageSlider from './ImageSlider'

const WrappedComponent = () => {
    const [showGallery, setShowGallery] = useState(null);
    const [currentImageIndex, setCurrentImageIndex] = useState(null);
    const [{ images }, dispatch] = useContext(StoreContext)
    const gallery = useRef();

    useEffect(() => {
        let userImages = localStorage.getItem('images')

        console.log(userImages)
        
        if (images.length === 0 && userImages) {
            dispatch({ type: 'SET_IMAGES', payload: JSON.parse(userImages) })
        } 

    }, [dispatch, images]);

    console.log(images)

    return (
        <div className="dashboard">
            <div className="gallery" ref={gallery}>
                {
                    images && 
                    images.map((image, i) => image ? <MasonryImg src={process.env.REACT_APP_CDN_URL + '/' + image} key={image} index={i} setIndex={setCurrentImageIndex} showGallery={setShowGallery}/> : null)
                }
            </div>
            {showGallery && <ImageSlider images={images} currentImageIndex={currentImageIndex} showGallery={setShowGallery}/>}
        </div>
    )
}

export default React.memo(WrappedComponent)