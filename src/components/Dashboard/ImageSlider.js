import React, { useEffect, useState, useRef } from 'react'
import './ImageSlider.scss'

export default ({ images, currentImageIndex, showGallery }) => {
    const [currentImgCoordinates, setCurrentImgCoordinates] = useState(null)
    const slider = useRef()

    useEffect(() => {
        if (currentImgCoordinates) {
            slider.current.scrollTo(currentImgCoordinates, 0, 'smooth')
            setCurrentImgCoordinates(false)
        }
    }, [currentImgCoordinates])

    return (
        <div className="slider" onClick={() => showGallery(false)}>
            <div className="slider__container flex align-center" ref={slider}>
                {
                    images.map((img, i) => {
                        return <div className="image-container">
                            <img 
                                key={img}
                                className="slider__image"
                                src={process.env.REACT_APP_CDN_URL + '/' + img} 
                                alt="" 
                                onLoad={
                                    e => {
                                        if (i === currentImageIndex) {
                                            let coordinates = e.target.getBoundingClientRect().left
                                            setCurrentImgCoordinates(coordinates)
                                        }
                                    }
                                }
                            />
                        </div>
                    })
                } 
            </div>
        </div>
    )
}