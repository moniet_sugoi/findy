import React, { useRef } from 'react'
import './MasonryImg.scss'

const WrappedComponent = ({ src, showGallery, setIndex, index}) => {
    const image = useRef(null) 

    const resizeImage = () => {
        const grid = document.querySelector(".gallery");
        const rowHeight = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-auto-rows'));
        const rowGap = parseInt(window.getComputedStyle(grid).getPropertyValue('grid-row-gap'));

        let height = image.current.getBoundingClientRect().height + rowGap
        const rowSize = rowHeight + rowGap

        let divisions = Math.floor(height / rowSize)

        image.current.style.gridRow = `span ${divisions}`
        image.current.style.height = '100%'
    }

    const handleClick = () => {
        setIndex(index)
        showGallery(true)
    }
    
    const handleLoad = e => {
        resizeImage()
        e.target.style.opacity = 1
    }

    // useEffect(() => {
    //     window.addEventListener('resize', () => {
    //         setTimeout(() => resizeImage(), 500) 
    //     })
    // }, []);

    return (
        <img src={src} alt="" ref={image} className="masonry-img" onClick={() => handleClick()} onLoad={handleLoad} />
    )
}

export default React.memo(WrappedComponent)