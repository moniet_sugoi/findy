import React, { useState, useEffect, useRef, useContext  } from 'react'

// components
import './Snap.scss'
import LinkArrow from '../../common/LinkArrow'
import { ReactComponent as Cross } from '../../assets/icons/cross.svg'

// utils
import { initVideo, clearphoto, captureImage } from '../../utils/RTC'
import Modal from '../../common/Modal'
import Icon from '../../common/Icon'
import { findImages } from '../../utils/api'
import { dataURItoBlob } from '../../utils/dataURItoBlob'
import Loading from '../../common/Loading'
import { useHistory } from 'react-router-dom'
import { StoreContext } from '../../store'

export default () => {
    const [streaming, setStreaming] = useState(false)
    const [snapTaken, setSnapTaken] = useState(false)
    const [modal, setModal] = useState(false)
    const [loading, setLoading] = useState(false)

    const videoElement = useRef(null)
    const imageElement = useRef(null)
    const canvasElement = useRef(null)
    const snapButton = useRef(null)

    const history = useHistory()

    const [state, dispatch] = useContext(StoreContext)

    useEffect(() => {
        if (!streaming) initVideo(
            videoElement.current, 
            imageElement.current, 
            canvasElement.current, 
            snapButton.current, 
            streaming, 
            setStreaming, 
            setSnapTaken
        )

        if (snapTaken) {
            snapButton.current.addEventListener('click', () => {
                snapButton.current.classList.add('disabled')

                setTimeout(() => {
                    snapButton.current.classList.remove('disabled')
                }, 500)
                clearphoto(canvasElement.current, imageElement.current, setSnapTaken)
            }, false)
            
            imageElement.current.style.display = 'block'
        } else {
            snapButton.current.addEventListener('click', (e) => {
                e.preventDefault()
                snapButton.current.classList.add('disabled')

                setTimeout(() => {
                    snapButton.current.classList.remove('disabled')
                }, 500)
                captureImage(videoElement.current, canvasElement.current, imageElement.current, setSnapTaken)
            })

            imageElement.current.style.display = 'none'
        }
    
    },[snapTaken, streaming]) 


    return (
        <>
            <div className="column flex-center">
                <div className="snap-container camera column flex-center">
                <h4 className="text-center header">Take a picture of yourself <span role="img" aria-label="upside down smiley face" >&#128247;</span> <br /> <small>We'll use it to find your pictures</small></h4>
                <div className="camera__container">
                    <video className="video" alt="captured image displayed here" ref={videoElement}></video>
                    <img ref={imageElement} className="image" alt="The screen capture will appear in this box."></img>
                </div>
                    {!snapTaken && <button ref={snapButton} className="camera__button button">Snap</button>}
                    {snapTaken && <button ref={snapButton} className="camera__button button">Retake</button>}
                <LinkArrow handleClick={() => setModal(true)}>
                    next
                </LinkArrow>
                </div>
                <canvas ref={canvasElement} className="canvas"></canvas>
                {
                    modal && snapTaken &&
                        <Modal hideModal={() => setModal(false)}> 
                            <div className="modal__confirmation column flex-center">
                                <div className="modal__escape-btn">
                                    <Icon action={() => setModal(false)}><img src={require('../../assets/icons/cross.svg')} alt="escape modal button"/></Icon>
                                </div>
                                <div className="modal__icon flex">
                                    <span role="img">&#128512;</span>
                                    <span role="img">&#128526;</span>
                                </div>
                                <h2 className="fs-small fw-bold">Are you happy with your picture</h2>
                                <button className="button modal__success-btn" onClick={() => {
                                    setLoading(true)

                                    let userImage = localStorage.getItem('userImage')
                                    let blob = dataURItoBlob(userImage)
                                    let fd = new FormData()

                                    fd.append('image-0', blob)

                                    findImages(fd)
                                        .then(res => {
                                            setLoading(false)
                                            localStorage.setItem('loggedIn', true)
                                            localStorage.setItem('images', JSON.stringify(res.data))
                                            dispatch({ type: 'SET_IMAGES', payload: res.data })
                                            setTimeout(() => history.push('/dashboard'), 250)
                                        }).catch(err => console.log(err))
                                }}>Yes</button>
                            </div>
                        </Modal>
                }

                {
                    modal && !snapTaken &&
                        <Modal hideModal={() => setModal(false)}> 
                            <div className="modal__confirmation column flex-center">
                                <div className="modal__icon">
                                    &#128579;
                                </div>
                                <h2 className="fs-small fw-bold">You have to take a picture to continue</h2>
                            </div>
                        </Modal>
                }
            </div>
            <Loading loading={loading} /> 
        </>
    )
}