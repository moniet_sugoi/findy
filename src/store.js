import React, { useReducer } from 'react'

export const StoreContext = React.createContext()

const initialState = {
    loading: false,
    loggedIn: false,
    images: [],
    hideNav: false
}

const reducer = (state, action) => {
    switch (action.type) {
        case 'SET_LOADING':
            return { ...state, loading: action.payload }
        case 'SET_IMAGES':
            return { ...state, images: action.payload }
        case 'SET_LOGGED_IN':
            return { ...state, loggedIn: action.payload }
        default:
            return state
    }
}

const StoreProvider = ({ children }) => 
    <StoreContext.Provider value={useReducer(reducer, initialState)}>
        { children }
    </StoreContext.Provider>

export default StoreProvider

