import React, { useEffect, useRef } from 'react' 
import ReactDOM from 'react-dom'

import './Modal.scss'

export default ({ hideModal, children }) => {
    const div = document.createElement('div')
    const root = document.querySelector('#root')
    const app = document.querySelector('.app')
    const modal = useRef(null)

    useEffect(() => {
        root.appendChild(div)
        app.style.filter = 'blur(5px)'
        modal.current.style.opacity = 1

        return () => {
            app.style.filter = 'none'
        }
    }, [div, hideModal, root]) // eslint-disable-line

    const handleClick = () => {
        app.style.filter = 'blur(0px)'
        hideModal()
    }
    

    return ReactDOM.createPortal(
        <div className="modal flex-center" onClick={() => handleClick()} ref={modal}>
            {children}
        </div>
    , div)
}