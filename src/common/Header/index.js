import React, { useContext, useRef, useEffect } from 'react'
import { StoreContext } from '../../store'

import './Header.scss'

export default () => {
    const [{ loggedIn }, dispatch] = useContext(StoreContext)
    const navbar = useRef()

    useEffect(() => {
        if (loggedIn) {
            navbar.current.style.display = 'flex'
            navbar.current.style.opacity = 1
        }
    }, [loggedIn]);

    console.log(loggedIn)

    return (
        <header className="flex" ref={navbar}>
            <div className="logo flex flex-center">
                <img 
                    alt="logo" 
                    src={require("../../assets/logo.svg")} 
                />
            </div>
        </header>
    )
}