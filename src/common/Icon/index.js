import React from 'react'
import './Icon.scss'

export default ({ children, action }) => 
    <button className="icon" onClick={() => action()}>
        {children}
    </button>