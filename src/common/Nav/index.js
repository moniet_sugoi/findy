import React, { useEffect, useContext, useRef } from 'react'
import { NavLink, Link, BrowserRouter as Router, useHistory } from 'react-router-dom'

import './Nav.scss'
import { StoreContext } from '../../store'

export default () => {
    const [{ loggedIn }, dispatch] = useContext(StoreContext)
    const navbar = useRef()

    console.log(loggedIn)

    useEffect(() => {
        if (loggedIn) {
            navbar.current.style.display = 'flex'
            navbar.current.style.opacity = 1
        }
    }, [navbar, loggedIn])

    return (

            <nav className="navbar flex" ref={navbar}>
                <div className="navbar__icon flex">
                    <NavLink to="/dashboard" activeClassName="active-icon" className="flex flex-center">
                        <svg xmlns="http://www.w3.org/2000/svg" stroke="#fff" width="24" height="24"><path d="M12 2a1 1 0 00-.71.297l-10.087 8.8A.5.5 0 001 11.5a.5.5 0 00.5.5H4v8a1 1 0 001 1h4a1 1 0 001-1v-6h4v6a1 1 0 001 1h4a1 1 0 001-1v-8h2.5a.5.5 0 00.5-.5.5.5 0 00-.203-.402l-10.08-8.795a1 1 0 00-.006-.006A1 1 0 0012 2z"/></svg>
                    </NavLink>
                </div>
                <div className="navbar__icon flex">
                    <NavLink exact to="/" activeClassName="active-icon" className="flex flex-center">
                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24">
                        <path fill="none" d="M0 0h24v24H0V0z"/>
                        <path fill="#fff" strokeWidth="0.05" d="M20 4h-3.17L15 2H9L7.17 4H4c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V6c0-1.1-.9-2-2-2zm0 14H4V6h4.05l1.83-2h4.24l1.83 2H20v12zM12 7c-2.76 0-5 2.24-5 5s2.24 5 5 5 5-2.24 5-5-2.24-5-5-5zm0 8c-1.65 0-3-1.35-3-3s1.35-3 3-3 3 1.35 3 3-1.35 3-3 3z"/>
                    </svg>
                    </NavLink>
                </div>
                {/* <div className="navbar__icon flex">
                    <NavLink exact to="/" activeClassName="active-icon" className="flex flex-center">
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24">
                            <path fill="none" d="M0 0h24v24H0V0z"/>
                            <path stroke="#fff" strokeWidth="0.5" d="M11 7h2v2h-2zm0 4h2v6h-2zm1-9C6.48 2 2 6.48 2 12s4.48 10 10 10 10-4.48 10-10S17.52 2 12 2zm0 18c-4.41 0-8-3.59-8-8s3.59-8 8-8 8 3.59 8 8-3.59 8-8 8z"/>
                        </svg>
                    </NavLink>
                </div> */}
            </nav>

    )
}

