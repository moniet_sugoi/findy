import React, { useRef, useEffect, useContext } from 'react'

import './Loading.scss'

const LoaderCircle = ({ number }) => <div className={`loading__circle circle-${number}`}></div>

export default ({ children, loading }) => {
    const container = useRef(null)

    const numberOfCircles = 4;
    let loaderCircles = [];

    for (let n = 0; n < numberOfCircles; n++) {
        loaderCircles.push(<LoaderCircle key={n} number={n+1} />);
    }

    useEffect(() => {
        if (container) {
            if (loading) {
                container.current.style.opacity = 1
                container.current.style.display = 'flex'
            } else {
                container.current.style.opacity = 0
                container.current.style.display = 'none'
            }
        }
    }, [loading])
      
    return (
        <div className="loading column flex-center" ref={container}>
            <div className="loading__icon flex">
                {loaderCircles}
            </div>
            <h4 className="ta-center fs-base fw-bold mb-zero">We're finding your images...</h4>
            <small className="fs-small">This will take a moment</small>
        </div>
    )
}

