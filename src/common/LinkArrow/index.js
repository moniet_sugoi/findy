import React from 'react'
import { Link } from 'react-router-dom'

import './LinkArrow.scss'

export default ({ handleClick, children }) => 
<span className="link-arrow">
    <button onClick={ () => handleClick()}>
        <small>{children}</small> &#8594;	
    </button>
</span>
    
