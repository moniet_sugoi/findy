import React from 'react';
import logo from './logo.svg';
import './App.css';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import Snap from './components/Snap';
import StoreProvider from './store';
import Dashboard from './components/Dashboard';
import Layout from './components/Layout';

const routes = [
  {
    path: '/dashboard',
    component: Dashboard
  },
  {
    path: '/',
    component: Snap
  }
] 

function App() {
  return (
    <div className="app">
      <StoreProvider>
          <Router>
            <Layout>
              <Switch>
                {
                  routes.map(({ path, component: Component }) => <Route key={path} path={path} component={Component} />)
                }
              </Switch>
            </Layout>
          </Router>        
      </StoreProvider>
    </div>
  );
}

export default App;
