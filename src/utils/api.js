const baseUrl = process.env.REACT_APP_BASE_URL
const findImagesUrl = `${baseUrl}/find-images`

export const findImages = (image) => {
    return fetch(findImagesUrl, {
        method: 'POST',
        body: image
    }).then(res => res.json())
}

