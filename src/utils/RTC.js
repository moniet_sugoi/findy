let width = 450,
height = 0

const streamVideo = async (video) => {
    let stream = await navigator.mediaDevices.getUserMedia({ video: true, audio: false })

    try {
        video.srcObject = stream;
        video.play();
    } catch (err) {
        console.log(err)
    }
        
}

export const clearphoto = (canvas, image, setSnapTaken) => {
    canvas.width = 0
    canvas.height = 0

    var data = canvas.toDataURL('image/png');
    image.setAttribute('width', width*2);
    image.setAttribute('height', height*2);
    image.setAttribute('src', data);

    setSnapTaken(false)
}

export const captureImage = (video, canvas, image, setSnapTaken) => {
    let context = canvas.getContext('2d');
    if (width) {
        canvas.width = width;
        canvas.height = height;
        context.drawImage(video, 0, 0, width, height);
    
        var data = canvas.toDataURL('image/jpg', 0.5);
        localStorage.setItem('userImage', data)
        image.setAttribute('src', data);

        if (data) {
            setSnapTaken(true)
        }
    } else {
        clearphoto(canvas, image);
    }
}

export const initVideo = async (video, image, canvas, snapButton, streaming, setStreaming, setSnapTaken) => {

    await streamVideo(video)

    video.addEventListener('canplay', function(ev){
        if (!streaming) {
          height = video.videoHeight / (video.videoWidth/width);
        
          video.setAttribute('width', width);
          video.setAttribute('height', height);
          canvas.setAttribute('width', width);
          canvas.setAttribute('height', height);
          setStreaming(true)
        }
      }, false);

      video.addEventListener('click', (e) => video.play())
}